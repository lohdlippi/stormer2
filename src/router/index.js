import Vue from 'vue';
import Router from 'vue-router';
import Landing from '@/components/Landing';
import Main from '@/components/Main';
import Terms from '@/components/Terms';
import Privacy from '@/components/Privacy';

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Landing',
      component: Landing
    },
    {
      path: '/terms',
      name: 'Terms',
      component: Terms
    },
    {
      path: '/privacy',
      name: 'Privacy',
      component: Privacy
    },
    {
      path: '/main',
      name: 'main',
      component: Main,
      beforeEnter: (to, from, next) => {
        const decoded = window.localStorage.getItem('stormerapp')
        if (decoded) {
          const currentTime = new Date().getTime() / 1000
          if (currentTime > decoded.exp) {
            // expired
            next('/')
          } else {
            next()
          }
        } else {
          next('/')
        }
      }
    }
  ]
});
