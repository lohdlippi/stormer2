// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import Icon from 'vue-awesome/components/Icon'
import VueFeatherIcon from 'vue-feather-icon'
import VModal from 'vue-js-modal'
import VueAnalytics from 'vue-analytics'
// globally (in your main .js file)
Vue.component('icon', Icon)
Vue.use(VModal)

Vue.use(VueFeatherIcon)
Vue.config.productionTip = false

Vue.use(VueAnalytics, {
  id: 'UA-88802350-1',
  router,
  autoTracking: {
    pageviewOnLoad: false
  },
  ignoreRoutes: []
})
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
})
